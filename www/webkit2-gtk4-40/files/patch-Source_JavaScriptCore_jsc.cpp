Index: Source/JavaScriptCore/jsc.cpp
--- Source/JavaScriptCore/jsc.cpp.orig	2023-02-20 09:22:05 UTC
+++ Source/JavaScriptCore/jsc.cpp
@@ -80,6 +80,7 @@
 #include "WasmCapabilities.h"
 #include "WasmFaultSignalHandler.h"
 #include "WasmMemory.h"
+#include <locale.h> /* LC_ALL */
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
