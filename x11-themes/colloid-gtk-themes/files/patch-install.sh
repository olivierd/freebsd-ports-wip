--- install.sh.orig	2023-08-12 15:52:57 UTC
+++ install.sh
@@ -107,8 +107,6 @@ install() {
   echo "[X-GNOME-Metatheme]" >>                                                              "${THEME_DIR}/index.theme"
   echo "GtkTheme=${2}${3}${4}${5}${6}" >>                                                    "${THEME_DIR}/index.theme"
   echo "MetacityTheme=${2}${3}${4}${5}${6}" >>                                               "${THEME_DIR}/index.theme"
-  echo "IconTheme=Tela-circle${ELSE_DARK:-}" >>                                              "${THEME_DIR}/index.theme"
-  echo "CursorTheme=${2}-cursors" >>                                                         "${THEME_DIR}/index.theme"
   echo "ButtonLayout=close,minimize,maximize:menu" >>                                        "${THEME_DIR}/index.theme"
 
   mkdir -p                                                                                   "${THEME_DIR}/gnome-shell"
@@ -163,11 +161,11 @@ install() {
   mkdir -p                                                                                   "${THEME_DIR}-hdpi/xfwm4"
   cp -r "${SRC_DIR}/assets/xfwm4/assets${ELSE_LIGHT:-}${scheme}${window}-hdpi/"*.png         "${THEME_DIR}-hdpi/xfwm4"
   cp -r "${SRC_DIR}/main/xfwm4/themerc${ELSE_LIGHT:-}"                                       "${THEME_DIR}-hdpi/xfwm4/themerc"
-  sed -i "s/button_offset=6/button_offset=9/"                                                "${THEME_DIR}-hdpi/xfwm4/themerc"
+  sed -i "" -e "s/button_offset=6/button_offset=9/"                                                "${THEME_DIR}-hdpi/xfwm4/themerc"
   mkdir -p                                                                                   "${THEME_DIR}-xhdpi/xfwm4"
   cp -r "${SRC_DIR}/assets/xfwm4/assets${ELSE_LIGHT:-}${scheme}${window}-xhdpi/"*.png        "${THEME_DIR}-xhdpi/xfwm4"
   cp -r "${SRC_DIR}/main/xfwm4/themerc${ELSE_LIGHT:-}"                                       "${THEME_DIR}-xhdpi/xfwm4/themerc"
-  sed -i "s/button_offset=6/button_offset=12/"                                               "${THEME_DIR}-xhdpi/xfwm4/themerc"
+  sed -i "" -e "s/button_offset=6/button_offset=12/"                                               "${THEME_DIR}-xhdpi/xfwm4/themerc"
 
   mkdir -p                                                                                   "${THEME_DIR}/plank"
   if [[ "$color" == '-Light' ]]; then
@@ -411,30 +409,12 @@ function has_command() {
   command -v $1 > /dev/null
 }
 
-#  Install needed packages
-install_package() {
-  if [ ! "$(which sassc 2> /dev/null)" ]; then
-    echo sassc needs to be installed to generate the css.
-    if has_command zypper; then
-      sudo zypper in sassc
-    elif has_command apt-get; then
-      sudo apt-get install sassc
-    elif has_command dnf; then
-      sudo dnf install sassc
-    elif has_command dnf; then
-      sudo dnf install sassc
-    elif has_command pacman; then
-      sudo pacman -S --noconfirm sassc
-    fi
-  fi
-}
-
 tweaks_temp() {
   cp -rf "${SRC_DIR}/sass/_tweaks.scss" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 compact_size() {
-  sed -i "/\$compact:/s/false/true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+  sed -i "" -e "s|compact: 'false|compact: 'true|" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 color_schemes() {
@@ -450,34 +430,34 @@ color_schemes() {
         scheme_color='gruvbox'
         ;;
     esac
-    sed -i "/\@import/s/color-palette-default/color-palette-${scheme_color}/" "${SRC_DIR}/sass/_tweaks-temp.scss"
-    sed -i "/\$colorscheme:/s/default/${scheme_color}/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+    sed -i "" -e "s/import 'color-palette-default/import 'color-palette-${scheme_color}|" "${SRC_DIR}/sass/_tweaks-temp.scss"
+    sed -i "" -e "s/colorscheme: 'default/colorscheme: '${scheme_color}/" "${SRC_DIR}/sass/_tweaks-temp.scss"
   fi
 }
 
 blackness_color() {
-  sed -i "/\$blackness:/s/false/true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+  sed -i "" -e "s/blackness: 'false/blackness: 'true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 border_rimless() {
-  sed -i "/\$rimless:/s/false/true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+  sed -i "" -e "s/rimless: 'false/rimless: 'true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 normal_winbutton() {
-  sed -i "/\$window_button:/s/mac/normal/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+  sed -i "" -e "s/window_button: 'mac/window_button: 'normal/" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 float_panel() {
-  sed -i "/\$float:/s/false/true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+  sed -i "" -e "s/float: 'false/float: 'true/" "${SRC_DIR}/sass/_tweaks-temp.scss"
 }
 
 gnome_shell_version() {
   cp -rf "${SRC_DIR}/sass/gnome-shell/_common.scss" "${SRC_DIR}/sass/gnome-shell/_common-temp.scss"
 
-  sed -i "/\widgets/s/40-0/${GS_VERSION}/" "${SRC_DIR}/sass/gnome-shell/_common-temp.scss"
+  sed -i "" -e "s/widgets-40-0/widgets-${GS_VERSION}/" "${SRC_DIR}/sass/gnome-shell/_common-temp.scss"
 
   if [[ "${GS_VERSION}" == '3-28' ]]; then
-    sed -i "/\extensions/s/40-0/${GS_VERSION}/" "${SRC_DIR}/sass/gnome-shell/_common-temp.scss"
+    sed -i "" -e "s/extensions-40-0/extensions-${GS_VERSION}/" "${SRC_DIR}/sass/gnome-shell/_common-temp.scss"
   fi
 }
 
@@ -509,7 +489,7 @@ theme_color() {
         theme_color='grey'
         ;;
     esac
-    sed -i "/\$theme:/s/default/${theme_color}/" "${SRC_DIR}/sass/_tweaks-temp.scss"
+    sed -i "" -e "s/theme: 'default/theme: '${theme_color}/" "${SRC_DIR}/sass/_tweaks-temp.scss"
   fi
 }
 
@@ -629,7 +609,7 @@ install_theme() {
   done
 
   if (which xfce4-popup-whiskermenu 2> /dev/null); then
-    sed -i "s|.*menu-opacity=.*|menu-opacity=95|" "$HOME/.config/xfce4/panel/whiskermenu"*".rc"
+    sed -i "" -e "s|.*menu-opacity=.*|menu-opacity=95|" "$HOME/.config/xfce4/panel/whiskermenu"*".rc"
   fi
 
   if (pgrep xfce4-session &> /dev/null); then
@@ -671,7 +651,7 @@ if [[ "$uninstall" == 'true' ]]; then
     echo && uninstall_theme && uninstall_link
   fi
 else
-  clean_theme && install_package && tweaks_temp && gnome_shell_version && install_theme
+  clean_theme && tweaks_temp && gnome_shell_version && install_theme
   if [[ "$libadwaita" == 'true' ]]; then
     uninstall_link && link_theme
   fi
