--- install.sh.orig	2022-09-28 11:12:03 UTC
+++ install.sh
@@ -1,6 +1,6 @@
 #! /usr/bin/env bash
 
-REPO_DIR="$(dirname "$(readlink -m "${0}")")"
+REPO_DIR="$(dirname "$(realpath -q "${0}")")"
 source "${REPO_DIR}/core.sh"
 
 usage() {
