--- gtkrc.sh.orig	2023-01-24 15:24:19 UTC
+++ gtkrc.sh
@@ -289,25 +289,25 @@ make_gtkrc() {
   fi
 
   cp -r "${GTKRC_DIR}/gtkrc${ELSE_DARK:-}"                                     "${THEME_DIR}/gtk-2.0/gtkrc"
-  sed -i "/tooltip_bg_color/s/#616161/${tooltip_bg}/"                          "${THEME_DIR}/gtk-2.0/gtkrc"
+  sed -i "" -e "s/tooltip_bg_color:#616161/tooltip_bg_color:${tooltip_bg}/"                          "${THEME_DIR}/gtk-2.0/gtkrc"
 
   if [[ "${color}" == '-Dark' ]]; then
-    sed -i "s/#3281EA/${theme_color}/g"                                        "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/bg_color/s/#212121/${background_dark}/"                           "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/base_color/s/#2C2C2C/${base_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/titlebar_bg_color/s/#2C2C2C/${titlebar_dark}/"                    "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/menu_color/s/#3C3C3C/${menu_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/text_color/s/#FFFFFF/${text_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/fg_color/s/#FFFFFF/${text_dark}/"                                 "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/titlebar_fg_color/s/#FFFFFF/${text_dark}/"                        "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/#3281EA/${theme_color}/g"                                        "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/bg_color:#212121/bg_color:${background_dark}/"                           "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/base_color:#2C2C2C/base_color:${base_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/titlebar_bg_color:#2C2C2C/titlebar_bg_color:${titlebar_dark}/"                    "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/menu_color:#3C3C3C/menu_color:${menu_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/text_color:#FFFFFF/text_color:${text_dark}/"                               "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/fg_color:#FFFFFF/fg_color:${text_dark}/"                                 "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/titlebar_fg_color:#FFFFFF/titlebar_fg_color:${text_dark}/"                        "${THEME_DIR}/gtk-2.0/gtkrc"
   else
-    sed -i "s/#1A73E8/${theme_color}/g"                                        "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/bg_color/s/#F2F2F2/${background_light}/"                          "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/base_color/s/#FFFFFF/${base_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/titlebar_bg_color/s/#FFFFFF/${titlebar_light}/"                   "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/menu_color/s/#FFFFFF/${menu_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/text_color/s/#212121/${text_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/fg_color/s/#212121/${text_light}/"                                "${THEME_DIR}/gtk-2.0/gtkrc"
-    sed -i "/titlebar_fg_color/s/#212121/${text_light}/"                       "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/#1A73E8/${theme_color}/g"                                        "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/bg_color:#F2F2F2/bg_color:${background_light}/"                          "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/base_color:#FFFFFF/base_color:${base_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/titlebar_bg_color:#FFFFFF/titlebar_bg_color:${titlebar_light}/"                   "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/menu_color:#FFFFFF/menu_color:${menu_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/text_color:#212121/text_color:${text_light}/"                              "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/fg_color:#212121/fg_color:${text_light}/"                                "${THEME_DIR}/gtk-2.0/gtkrc"
+    sed -i "" -e "s/titlebar_fg_color:#212121/titlebar_fg_color:${text_light}/"                       "${THEME_DIR}/gtk-2.0/gtkrc"
   fi
 }
