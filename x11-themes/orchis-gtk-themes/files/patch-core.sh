--- core.sh.orig	2023-01-24 15:24:19 UTC
+++ core.sh
@@ -1,5 +1,5 @@
 
-REPO_DIR="$(dirname "$(readlink -m "${0}")")"
+REPO_DIR="$(dirname "$(realpath -q "${0}")")"
 SRC_DIR="$REPO_DIR/src"
 
 source "${REPO_DIR}/gtkrc.sh"
@@ -64,9 +64,6 @@ install() {
   echo "[X-GNOME-Metatheme]" >>                                                 "$THEME_DIR/index.theme"
   echo "GtkTheme==${2}${3}${4}${5}${6}" >>                                      "$THEME_DIR/index.theme"
   echo "MetacityTheme==${2}${3}${4}${5}${6}" >>                                 "$THEME_DIR/index.theme"
-  echo "IconTheme=Tela-circle${ELSE_DARK:-}" >>                                 "$THEME_DIR/index.theme"
-  echo "CursorTheme=Vimix${ELSE_DARK:-}" >>                                     "$THEME_DIR/index.theme"
-  echo "ButtonLayout=close,minimize,maximize:menu" >>                           "$THEME_DIR/index.theme"
 
   mkdir -p                                                                      "$THEME_DIR/gnome-shell"
   cp -r "$SRC_DIR/gnome-shell/pad-osd.css"                                      "$THEME_DIR/gnome-shell"
@@ -236,77 +233,77 @@ tweaks_temp() {
 }
 
 change_radio_color() {
-  sed -i "/\$check_radio:/s/default/primary/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/check_radio: 'default/check_radio: 'primary/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_compact_panel() {
-  sed -i "/\$panel_style:/s/float/compact/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/panel_style: 'float/panel_style: 'compact/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_solid() {
-  sed -i "/\$opacity:/s/default/solid/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/opacity: 'default/opacity: 'solid/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_black() {
-  sed -i "/\$blackness:/s/false/true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/blackness: 'false/blackness: 'true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_mac() {
-  sed -i "/\$mac_style:/s/false/true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/mac_style: 'false/mac_style: 'true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 round_corner() {
-  sed -i "/\$default_corner:/s/12px/${corner}/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/default_corner: 12px/default_corner: ${corner}/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_submenu() {
-  sed -i "/\$submenu_style:/s/false/true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/submenu_style: 'false/submenu_style: 'true/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_nord() {
-  sed -i "/\@import/s/color-palette-default/color-palette-nord/" ${SRC_DIR}/_sass/_tweaks-temp.scss
-  sed -i "/\$colorscheme:/s/default/nord/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/@import color-palette-default/@import color-palette-nord/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/colorscheme: 'default/colorscheme: 'nord/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_dracula() {
-  sed -i "/\@import/s/color-palette-default/color-palette-dracula/" ${SRC_DIR}/_sass/_tweaks-temp.scss
-  sed -i "/\$colorscheme:/s/default/dracula/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/@import color-palette-default/@import color-palette-dracula/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+  sed -i "" -e "s/colorscheme: 'default/colorscheme: 'dracula/" ${SRC_DIR}/_sass/_tweaks-temp.scss
 }
 
 install_theme_color() {
   if [[ "$theme" != '' ]]; then
     case "$theme" in
       -Purple)
-        theme_color='purple'
+        theme_color="purple"
         ;;
       -Pink)
-        theme_color='pink'
+        theme_color="pink"
         ;;
       -Red)
-        theme_color='red'
+        theme_color="red"
         ;;
       -Orange)
-        theme_color='orange'
+        theme_color="orange"
         ;;
       -Yellow)
-        theme_color='yellow'
+        theme_color="yellow"
         ;;
       -Green)
-        theme_color='green'
+        theme_color="green"
         ;;
       -Teal)
-        theme_color='teal'
+        theme_color="teal"
         ;;
       -Grey)
-        theme_color='grey'
+        theme_color="grey"
         ;;
     esac
-    sed -i "/\$theme:/s/default/${theme_color}/" ${SRC_DIR}/_sass/_tweaks-temp.scss
+    sed -i "" -e "s/theme: 'default/theme: '${theme_color}/" ${SRC_DIR}/_sass/_tweaks-temp.scss
   fi
 }
 
 theme_tweaks() {
-  install_package; tweaks_temp
+  tweaks_temp
 
   if [[ "$panel" == "compact" || "$opacity" == 'solid' || "$blackness" == "true" || "$accent" == "true" || "$primary" == "true" || "$round" == "true" || "$macstyle" == "true" || "$submenu" == "true" || "$nord" == 'true' || "$dracula" == 'true' ]]; then
     tweaks='true'
