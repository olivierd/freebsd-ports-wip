--- lib/Services/EnvironmentSettings.vala.orig	2025-02-28 03:58:18 UTC
+++ lib/Services/EnvironmentSettings.vala
@@ -84,7 +84,7 @@ namespace Plank {
 
     public static unowned PantheonDesktopNotifications ? try_get_instance () {
       if (instance == null) {
-        var settings = try_create_settings ("org.pantheon.desktop.gala.notifications");
+        var settings = try_create_settings ("io.elementary.notifications");
         if (settings != null && ("do-not-disturb" in settings.settings_schema.list_keys ()))
           instance = (PantheonDesktopNotifications) Object.new (typeof (PantheonDesktopNotifications),
                                                                 "settings", settings, "bind-flags", SettingsBindFlags.GET | SettingsBindFlags.INVERT_BOOLEAN, null);
