--- src/vidcap/vidcap.c.orig	2020-02-11 11:24:26 UTC
+++ src/vidcap/vidcap.c
@@ -640,7 +640,7 @@ thread_func(void *data)
 	char filename[256], ext[32];
 	int i, j, ret, found;
 
-	fd = open(RAWFILE, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC | O_LARGEFILE, 0644);
+	fd = open(RAWFILE, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 0644);
 
 	if (!fd) {
 		compLogMessage("vidcap", CompLogLevelError,
@@ -788,7 +788,7 @@ vidcapToggle(CompDisplay     *d,
 		header.width = d->screens->width;
 		header.height = d->screens->height;
 
-		vd->fd = open(WCAPFILE, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC | O_LARGEFILE, 0644);
+		vd->fd = open(WCAPFILE, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 0644);
 
 		if (!vd->fd) {
 			compLogMessage("vidcap", CompLogLevelError,
