--- plugins/svg.c.orig	2020-02-11 11:21:50 UTC
+++ plugins/svg.c
@@ -32,7 +32,7 @@
 #include <librsvg/rsvg-cairo.h>
 #endif
 #ifndef LIBRSVG_FEATURES_H
-#include <librsvg/librsvg-features.h>
+#include <librsvg/rsvg-features.h>
 #endif
 
 #include <X11/Xatom.h>
