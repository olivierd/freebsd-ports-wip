--- meson_options.txt.orig	2024-12-16 12:21:54 UTC
+++ meson_options.txt
@@ -41,6 +41,13 @@ option(
 )
 
 option(
+  'examples',
+  type: 'boolean',
+  value: true,
+  description: 'Install examples'
+)
+
+option(
   'dbus_daemondir',
   type: 'string',
   description: 'Directory for installing the dbus-daemon'
