--- dbus/dbus-sysdeps-unix.c.orig	2024-12-16 12:21:54 UTC
+++ dbus/dbus-sysdeps-unix.c
@@ -62,6 +62,10 @@
 #include <grp.h>
 #include <arpa/inet.h>
 
+#ifdef __FreeBSD__
+#include <sys/ucred.h>
+#endif
+
 #ifdef HAVE_ERRNO_H
 #include <errno.h>
 #endif
@@ -2302,8 +2306,10 @@ _dbus_read_credentials_socket  (DBusSocket       clien
      * SCM_CREDS; it's implemented in GIO, but we don't use it in dbus at all,
      * because this is much less fragile.
      */
-#ifdef __OpenBSD__
+#if defined(__OpenBSD__)
     struct sockpeercred cr;
+#elif defined(__FreeBSD__)
+    struct xucred cr;
 #else
     struct ucred cr;
 #endif
